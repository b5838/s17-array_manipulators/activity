// console.log("Hello World!");

let students = [];
function addStudent(student){
    students.push(student);
    console.log(student + " was added to the student's list.")
}

function countStudents(students){
    console.log("There are a total of " + students.length + " students enrolled.");
}

function printStudents(){
        students.sort();
        students.forEach(function(student){
        console.log(student);
    })
}


function findStudent(find){
    let foundStudent = students.filter(function(student){
        return student.toLowerCase().includes(find.toLowerCase());
    })
    if(foundStudent.length == 1){
        console.log(foundStudent + " is an enrollee")
    }
    else if(foundStudent.length > 1){
        console.log(foundStudent + " are enrollees.")
    }
    else{
        console.log(find + " is not an enrollee")
    }
}

